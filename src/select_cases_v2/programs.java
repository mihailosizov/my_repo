package select_cases_v2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class programs {

	public static String select_cases(String inputFilePath, int rowAmount) throws IOException {
		String resultFilePath;
		Random random = new Random();

		File file = new File(inputFilePath);

		FileReader fileReader = new FileReader(file);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		List<String> inputLines = new ArrayList<>();
		List<String> outputLines = new ArrayList<>();

		// read the file to the input
		String line;
		while ((line = bufferedReader.readLine()) != null) {
			inputLines.add(line);
		}
		fileReader.close();
		bufferedReader.close();

		outputLines.add(inputLines.get(0)); // add first row containing column
											// names to the output

		// adding selected amount of random rows to the output
		for (int i = 0; i < rowAmount; i++) {
			int randomLineNumber = random.nextInt(inputLines.size() - 1) + 1; // generate
																				// random
																				// line
																				// number
																				// skipping
																				// the
																				// first
																				// title
																				// line
			outputLines.add(inputLines.get(randomLineNumber)); // add random
																// line to the
																// output
			inputLines.remove(randomLineNumber); // remove selected line from
													// the input
		}

		// get input file name
		String filePath = file.getCanonicalPath();
		int lastDotPosition = filePath.lastIndexOf(".");
		if (lastDotPosition > 0) {
			filePath = filePath.substring(0, lastDotPosition);
		}

		// get input file extension
		String fileExtension = file.getCanonicalPath();
		if (lastDotPosition == filePath.length()) {
			fileExtension = fileExtension.substring(lastDotPosition, file.getCanonicalPath().length());
		} else
			fileExtension = "";

		// write the results to the output text file
		resultFilePath = filePath + "_res" + fileExtension;
		Files.write(Paths.get(resultFilePath), outputLines);

		// rewrite input text file without selected rows
		Files.write(Paths.get(inputFilePath), inputLines);

		return resultFilePath;
	}

	public static String select_cases(String inputFilePath) throws IOException {
		return select_cases(inputFilePath, 10);
	}

}
