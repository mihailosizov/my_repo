package select_cases;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
 
class tests
{
        public static String select_cases(String path, Integer rowAmount)
        {
                String resultFilename = new String();
                Random random = new Random();
                               
                try {
                        File file = new File(path);            
                       
                        if (!file.exists()) {
                                return "file not exists";
                        }
                        
                        if (file.length() == 0) {
                                return "file is empty";
                        } 
                       
                        FileReader fileReader = new FileReader(file);
                        BufferedReader bufferedReader = new BufferedReader(fileReader);
                        List<String> inputLines = new ArrayList<String>();
                        List<String> outputLines = new ArrayList<String>();
                       
                        // read the file to the input
                        String line;
                        while ((line = bufferedReader.readLine()) != null)  {
                                inputLines.add(line);
                        }
                        fileReader.close();
                        bufferedReader.close();
                       
                        if (inputLines.size() <= 1) {
                                return "there no test cases left in the file";
                        }
                       
                        outputLines.add(inputLines.get(0));     // add first row containing column names to the output
                       
                        // adding selected amount of random rows to the output
                        int iterations = 0;
                        while (iterations < rowAmount) {
                                if (inputLines.size() <= 1) {
                                        System.out.println("Number of test cases in the input file is less then given output number");
                                        break;
                                }
                                int randomLineNumber = random.nextInt(inputLines.size() - 1) + 1;   // generate random line number skipping the first title line
                                outputLines.add(inputLines.get(randomLineNumber));      // add random line to the output
                                inputLines.remove(randomLineNumber); // remove selected line from the input
                                iterations++;
                        }
                                               
                        //get input file name
                        String fileName = file.getName();
                        int pos1 = fileName.lastIndexOf(".");
                        if (pos1 > 0) {
                                fileName = fileName.substring(0, pos1);
                        }
 
                        //get input file extension
                        String fileExtension = file.getName();
                        int pos2 = fileExtension.lastIndexOf(".");
                        if (pos2 == fileName.length()) {
                                fileExtension = fileExtension.substring(pos2, file.getName().length());
                        }
                        else fileExtension = "";
                       
                        //write the results to the output text file
                        resultFilename = file.getParent() + "\\" + fileName + "_res" + fileExtension;
                        FileWriter resultFileWriter = new FileWriter(resultFilename);
                        String[] outputToWrite = outputLines.toArray(new String[outputLines.size()]);
                        for (int i = 0; i < outputLines.size(); i++) {
                                resultFileWriter.write(outputToWrite[i] + "\r\n");
                        }
 
                        //rewrite input text file without selected rows                
                        FileWriter inputFileWriter = new FileWriter(path, false);
                        String[] inputToWrite = inputLines.toArray(new String[inputLines.size()]);
                        for (int i = 0; i < inputLines.size(); i++) {
                                inputFileWriter.write(inputToWrite[i] + "\n");
                        }
 
                        resultFileWriter.close();
                        inputFileWriter.close();
                                               
                } catch (IOException e) {
                        e.printStackTrace();
                }
                               
                return resultFilename;
        }
 
        public static String select_cases(String path)
        {
                return select_cases(path, 10);
        }
 
}
 
public class test_class {
 
        public static void main(String[] args) {
                String res = tests.select_cases("C:/Temp/file.txt", 1);
                System.out.println(res);
        }
}