package select_cases_v3;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
 
class tests
{
        public static String select_cases(String inputFilePath, Integer rowAmount) throws IOException
        {
            String resultFilePath;
            File file = new File(inputFilePath);            
            FileReader fileReader = new FileReader(file);
            Random random = new Random();
            
            // adding random line numbers from input file to array
            LineNumberReader  lnr = new LineNumberReader(fileReader);
            lnr.skip(Long.MAX_VALUE); // move line reader to the very end of file
            List<Integer> randomLineNumbers = new ArrayList<>();
            while (randomLineNumbers.size() < rowAmount) {
            	int randomLineNumber = random.nextInt(lnr.getLineNumber() - 1) + 1; // add random line to the list skipping the first title line
            	if (!randomLineNumbers.contains(randomLineNumber)) { // add only unique line numbers to the list
            		randomLineNumbers.add(randomLineNumber);
				}
            }
            lnr.close();
            fileReader.close();
                        
            // get input file name
            String filePath = file.getCanonicalPath();
            int lastDotPosition = filePath.lastIndexOf(".");
            if (lastDotPosition > 0) {
            	filePath = filePath.substring(0, lastDotPosition);
            }
 
            // get input file extension
            String fileExtension = file.getCanonicalPath();
            if (lastDotPosition == filePath.length()) {
            	fileExtension = fileExtension.substring(lastDotPosition, file.getCanonicalPath().length());
            }
            else fileExtension = "";
            
            resultFilePath = filePath + "_res" + fileExtension;
           
            // reading the input file line by line and write output and input to the new files
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            File outputFile = new File(resultFilePath);
            PrintWriter outputPrintWriter = new PrintWriter(outputFile);
            File newInputFile = new File(inputFilePath + ".tmp"); // write output to the temporary file before overwriting an existing input file
            PrintWriter newInputPrintWriter = new PrintWriter(newInputFile);
            String line;
            loop1: for (int index = 0; (line = bufferedReader.readLine()) != null; index++) {
				if (index == 0) { 								// add first title lines to both files
					outputPrintWriter.println(line);
					newInputPrintWriter.println(line);
					continue;
				}
                // if current loop index equals to any line number from random list, then add this line to output
                for (Integer randomLineNumber : randomLineNumbers) {
                    if (index == randomLineNumber) {
                        outputPrintWriter.println(line);
                        continue loop1;
                    }
                }
				newInputPrintWriter.println(line); // if any line number from random list does not equal to current loop index, then add this line to the the temporary file  
			}
            
            bufferedReader.close();
            fileReader.close();
            newInputPrintWriter.close();
            outputPrintWriter.close();

            Files.move(newInputFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.ATOMIC_MOVE); // rename temporary input file to existing input file
            return resultFilePath;
        }
        
        public static String select_cases(String inputFilePath) throws IOException
        {
    		return select_cases(inputFilePath, 10);
        }
 
}
 
public class test_class {
 
        public static void main(String[] args) throws IOException {
        	String inputFilePath = "D:/!MyJob/Parallels/AutoTesting-Task/file.txt";
        	System.out.println(tests.select_cases(inputFilePath, 3));
        }
}