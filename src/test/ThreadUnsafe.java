package test;

import org.testng.annotations.Test;

public class ThreadUnsafe {
	private static int[] accounts;
	private static int MAX = 1000;

	@Test
	public void init() {
		accounts = new int[] { 0, 0 };
	}

	@Test(dependsOnMethods = { "init" }, threadPoolSize = 1000, invocationCount = 1000, timeOut = 2000)
	public void swap() {
		int amount = (int) (MAX * Math.random());
		accounts[1] += amount;
		accounts[0] -= amount;
		System.out.println("Account[0]: " + accounts[0]);
		System.out.println("Account[1]: " + accounts[1]);
		int balance = checkBalance();
		assert(balance == 0);
	}

	public int checkBalance() {
		int sum = 0;
		for (int i = 0; i < accounts.length; i++) {
			sum += accounts[i];
		}
		System.out.println("Balance : " + sum);
		return sum;
	}

	/*
	 * @Test(timeOut = 10000, invocationCount = 1000, successPercentage = 98)
	 * public void waitForAnswer() { while (!success) { Thread.sleep(1000); } }
	 */

	public static void main(String[] args) {
		ThreadUnsafe tu = new ThreadUnsafe();
		tu.init();
		tu.swap();
	}
}